	// console.log("Hello");

	const getCube = 2 ** 3;
	console.log(`The cube of 2 is… ${getCube}`);

	const address = ["258", "Washington", "Ave NW", "California", "90011"];

	const [address1, address2, address3, address4, address5] = address;
	console.log(`I live at ${address1} ${address2} ${address3} ${address4} ${address5}`);

	const animal = {
		name : "Lolong",
		species : "Saltwater crocodile",
		weight : "1075 kgs",
		measurement : "20 ft 3 in"
	};

	const {name, species, weight, measurement} = animal;
	console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

	const numbers = [1,2,3,4,5];

	numbers.forEach(number => console.log(number));

	const reducedNumber = numbers.reduce((x, y) => x + y);
	console.log(reducedNumber);

	class Dog{
		constructor(name, age, breed){
			this.dogName = name;
			this.dogAge = age;
			this.dogBreed = breed;
		}
	}

	let dog = new Dog("Bob", "5", "Golden Retriever");
	console.log(dog);